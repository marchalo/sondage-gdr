Sondage sur la gestion des données de recherche
===============================================

Instruction
-----------

Votre participation à ce sondage est volontaire. Cela signifie que vous acceptez de
participer au projet sans aucune contrainte ou pression extérieure et que, par ailleurs,
vous êtes libre de décider de ne pas répondre à une question ou de mettre fin à votre
participation à tout moment.

En soumettant vos réponses au sondage, vous donnez votre consentement aux
comité GDR à utiliser les renseignements fournis pour présenter les résultats au sein de
l'institution ainsi qu’à conserver les données et à les partager. L’information que vous
partagerez demeurera strictement confidentielle : aucun nom, titre de poste (❓❓❓), adresse IP,
courriel ou toute autre information d’identification ne sera associée aux informations
fournies.

Ce questionnaire devrait prendre environ ❓❓❓ minutes à compléter. Le sondage sera ouvert
jusqu’au ❓❓❓.
Pour tout renseignement additionnel concernant ce sondage, vous pouvez communiquer
avec ❓❓❓ à l'adresse ❓❓❓.

Objectifs
---------

Afin de mieux connaître les enjeux liés aux pratiques de gestion des données de
recherche Cégep de Drummondville, nous menons une enquête auprès des différents groupes
en lien avec la recherche. Votre participation à ce sondage est importante et nous aidera à :
1.	déterminer comment les chercheurs·es du Cégep gèrent et partagent leurs
données de recherchei en ce moment;
2.	déterminer comment le Cégep peut offrir un soutien dans les activités liées à la
gestion des données.

Définitions
-----------

Dans le cadre du présent sondage:
- Un·e « chercheur·euse » peut désigner toute personne ayant travaillé sur un projet de
recherche et pouvant se déclarer légitimement l’auteur du projet. Les résultats de
cette recherche devaient être publiés dans un ouvrage savant. Cette définition inclue entre autre
les enseignants·es chercheurs·euses, les étudiants·es ainsi que les professionnel·le de
recherche.
- Un·e « collaborateur·trice technique » est toutes personnes ayant participé·e au déroulement
du projet, mais ne pouvant pas se déclarer légitimement l’auteur du projet. Cette déninition inclue
entre autre les techniciens·nes informatique et en documentation.
- Un « projet de recherche » peut désigner la recherche associée à l’étude d’une
hypothèse ou d’un groupe d’hypothèses (et l’ensemble de prédictions associées)
visant à répondre à une question de recherche distincte ou précise. Une même
subvention de recherche peut appuyer un projet de recherche ou de multiples
projets de recherche.  Dans le contexte du présent sondage, un projet de recherche
est associé à un ensemble de données de recherche distinct et est considéré
comme un sous-ensemble d’un programme de recherche, d’une activité de
recherche ou d’un domaine de recherche sur lequel ou laquelle vous travaillez.
- Les « données de recherche » peuvent être définies comme étant toutes les
données recueillies, observées, créées ou analysées pour produire les résultats
de recherche. Les données de recherche peuvent comprendre :
	- les données d’observation telles que les relevés de capteurs, les résultats
de sondage et les images;
	- les données expérimentales telles que les séquences génétiques et les
valeurs de champs magnétiques;
	- les données de simulation telles que les modèles climatiques ou
économiques et données obtenues par la méthode de Monte-Carlo;
	- les données dérivées ou compilées telles que l’exploration de textes et de
données, les bases de données compilées, les modèles 3D qui peuvent se
présenter sous différentes formes et comprendre notamment des textes, des
documents numériques, des modèles, des logiciels et des données propres
à une discipline ou à un instrument.
