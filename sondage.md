Questions
=========

Question 01
------------

Combien de projets de recherche avez-vous dirigés au cours des trois
dernières années à titre de chercheur·e principal·e ou de responsable de
projet? Sélectionnez une réponse.

1. Un ou deux projets de recherche
2. De trois à cinq projets de recherche
3. Plus de cinq projets de recherche
4. Je ne sais pas
5. Ne s’applique pas



Question 02
-----------

Combien d’espace utilisez-vous habituellement pour stocker les données
d’un projet de recherche? Sélectionnez une réponse.

1. Moins de 1 Go (Gigaoctet)
2. De 1 Go à moins de 10 Go
3. De 10 Go à moins de 50 Go
4. De 50 Go à moins de 500 Go
5. De 500 Go à moins de 1000 Go
6. De 1 To (teraoctet) à moins de 4 To
7. De 4 To à moins de 500 To
8. Plus de 500 To
9. Ne sais pas
10. Ne s’applique pas

Question 03
-----------

Lesquels des choix suivants décrivent le mieux les données de recherche que
vous produisez ou utilisez dans un projet de recherche typique? Choisissez toutes
les réponses qui s’appliquent.

1. Données géospatiales (p. ex., matrice, vecteur, grille, etc.)
2. Données propres à un instrument (p. ex., format de données pour un microscope confocal d’Olympus, imagerie infrarouge frontale [fichiers SEQ], IRMf, etc.)
3. Modèles (p. ex., 3D, statistique, de similitudes, macroéconomique, de causalité, etc.)
4. Multimédia (p. ex., JPEG, TIFF, MPEG, MP3, Quicktime, Bitmap, MP4, etc.)
5. Numérique (p. ex., CSV, MAT, XLS, SPSS, etc.)
6. Logiciel (p. ex., Java, C, Perl, Python, Ruby, PHP, R, etc.)
7. Texte (p. ex., TXT, DOC, PDF, RTF, HTML, XML, TEI, verbatim, examen des profils, dossiers médicaux, etc.)
8. Autre (p. ex., modèles 3D, conception graphique, BAM, CIF, FASTQ, FITS, CEL, IDAT, FASTA, PBD, BRK, DICOM, etc.)

Question 04
-----------

De quelle façon stockez-vous habituellement les données de recherche de
vos projets des trois dernières années? Choisissez toutes les réponses qui s’appliquent.

1. Clé USB
2. CD/DVD
3. Disque dur d’ordinateur de bureau
4. Disque dur d’ordinateur portable
5. Disque dur externe
6. Disque dur de l’instrument ou du capteur qui produit les données
7. Réseau du Cégep ou serveur d'un département
8. Solution infonuagique ou sur le web (p. ex., Dropbox, Google Drive, services en nuage d’Amazon, services en nuage de Microsoft, Teams)
9. Dépôt de données externe (p. ex., Dryad, Protein Data Bank, Git, Figshare, GenBank, Cancer Imaging Archive, HathiTrust, tDAR, Artstor).
10. Centre de calcul de haute performance (p. ex., CHP)/ grille informatique (p. ex., Calcul Québec)
11. Copies papier conservées (p. ex., dans une boîte ou un classeur)
12. Je ne sais pas
13. Autre

Question 05 ❓ Demander à Julie ❓
-----------

Existe-t-il suffisamment de documentation et de descriptions (p. ex.,
nom des fichiers, définition des variables et des champs, manuels de
codes, dictionnaires de données, métadonnées, scripts à exécuter) pour
qu’une personne qui ne fait pas partie de votre équipe puisse *comprendre*
et *utiliser* toutes vos données de recherche ?

1. Toujours
2. Habituellement
3. Rarement
4. Jamais
5. Ne sais pas

Question 06 ❓ Demander à Julie ❓
-----------

Existe-t-il suffisamment de documentation et de descriptions (p. ex.,
définition des variables et des champs, manuels de codes, dictionnaires de
données, métadonnées, scripts à exécuter) conservées dans un même fichier,
dossier ou document pour qu’une personne qui ne fait pas partie de votre
équipe de recherche puisse reproduire les méthodes à l'origine des données?

1. Toujours
2. Habituellement
3. Rarement
4. Jamais
5. Ne sais pas

Question 07 ✅ Demander à Julie si la séparation des types de données est pertinente ✅
-----------------

Dans la grille ci-dessous, précisez combien de temps vous conservez
habituellement et intentionnellement chaque type de données une fois le projet
terminé. La fin du projet peut correspondre, entre autres, à la publication de
résultats ou à l’octroi d’un brevet
- (a) Sources originales/ résultats du sondage/ données brutes
- (b) Données traitées/intermédiaires
- (c) Données prêtes à être publiées. Elles peuvent comprendre des renseignements
à l’appui tels que des métadonnées et de la documentation, des spectres ou des
méthodes de synthèse

1. Je ne conserve les données que pour la durée du projet
2. Moins de 3 ans
3. De 3 à 5 ans
4. De 6 à 10 ans
5. Plus de 10 ans
6. Jusqu’à ce que les données soient inaccessibles ou perdues

Question 08
-----------

De quelle(s) façon(s) partagez-vous actuellement vos données de
recherche? Choisissez toutes les réponses qui s’appliquent.

1. Je ne partage pas mes données
2. Je les partage uniquement avec les personnes qui en font directement la demande
3. Je les partage en ligne, mais leur accès est restreint
4. Je les partage par le biais des serveurs du Cégep (p. ex., sites web, espaces de stockage, etc.) ou sur mon site personnel
5. Je les fournis aux éditeurs à titre de matériel complémentaire
6. Je les verse dans un dépôt (p. ex., Dataverse, Dryad, Protein Data Bank, Git, Figshare, GenBank, Cancer Imaging Archive, HathiTrust, tDAR, Artstor)

Question 09 ✅❌ Lien avec question 07 (demande pour Julie) ❌✅
-----------

Précisez quel type de données vous partagez habituellement.

1. Je ne partage pas mes données
2. Données sources originales /résultats de sondage/ données brutes
3. Données traitées/intermédiaires
4. Données prêtes à être publiées. Elles peuvent comprendre des renseignements à l’appui
tels que des métadonnées et de la documentation, des spectres ou des méthodes de synthèse

Question 10
-----------

En supposant que vous souhaiteriez partager vos données à l’avenir, de
quelle(s) façon(s) le feriez-vous? Choisissez toutes les réponses qui s’appliquent.

1. Je n’ai pas l'intention de partager mes données
2. Je les partagerais uniquement avec les personnes qui en font personnellement la demande
3. Je les partagerais en ligne, mais leur accès serait restreint
4. Je les partagerais par le biais des serveurs du Cégep (p. ex., sites web, espaces de stockage) ou sur mon site personnel
5. Je les verserais en ligne dans le dépôt supporté par l’institution (Par exemple, Dataverse local)
6. Je les fournirais aux éditeurs à titre de matériel complémentaire
7. Je les verserais dans un dépôt (p. ex., Dryad, Protein Data Bank, Git, Figshare, GenBank, Cancer Imaging Archive, HathiTrust, tDAR, Artstor)

Question 11
-----------

S’il y a lieu, pour quelle(s) raison(s) refuseriez-vous de partager vos données
de recherche ainsi que les méthodes, outils ou algorithmes associés? Choisissez
toutes les réponses qui s’appliquent.

1. Aucune, je suis prêt à partager mes données
2. Elles sont incomplètes ou non terminées
3. Je souhaite encore en tirer profit (Je veux publier mes résultats avant de partager mes données, je prévois demander un brevet, ...)
4. Je ne possède ni les compétences techniques ni les connaissances nécessaires
5. Je ne détiens pas les droits me permettant de les partager; je dois respecter des obligations contractuelles
6. L’organisme de financement n’exige pas le partage des données
7. Je crois qu’elles ne devraient pas être partagées
8. Je ne savais pas que je pouvais les partager
9. Je ne suis pas certain si j'ai le droit de les partager
10. Je n’ai pas assez de temps pour le faire
11. Les données ne sont pas assez documentées pour qu’elles puissent être comprises et utilisées par d’autres personnes
12. Je manque de financement pour le faire
13. Elles ne sont pas utiles à d’autres personnes
14. Il n’y a pas d’endroit où les déposer
15. Il y a des enjeux juridiques, de protection des renseignements personnels ou de sécurité
16. Elles pourraient être mal utilisées
17. Je crains que mes données soient utilisées sans citation ou reconnaissance appropriée; je veux protéger mes droits de propriété intellectuelle
18. Autre

Question 12
-----------

Les plans de gestion des données abordent généralement des questions liées
aux types et aux formats de données de recherche, aux normes prescrites pour la
description des données, aux exigences éthiques et juridiques, aux plans relatifs à la
préservation, à l’accès, au partage et à la réutilisation ainsi qu'aux responsabilités
attribuées et aux ressources nécessaires.

Si l’on vous demandait de préparer un plan de gestion des données dans le cadre
d’une demande de subvention, quel énoncé parmi les suivants décrirait le mieux
votre situation?

1. Je serais en mesure de préparer seul un plan de gestion de données pour répondre à ces
types de questions
2. Je serais en mesure de préparer un plan de gestion de données répondant à ces questions,
mais je préférerais avoir de l’aide ou un guide pour m’assurer que ma demande soit
acceptée
3. J’aurais besoin d’aide ou d’un guide pour répondre adéquatement à certaines sections ou à
toutes les sections d’un tel plan

Question 13
-----------

Utilisez-vous vos propres données de recherche pour votre enseignement?

1. Oui
2. Non
3. Ne s’applique pas

Question 14
--------------

Si les plans de gestion des données étaient exigés dans les demandes de
subvention faites auprès des organismes tels que le CRSH, les IRSC et le CRSNG,
dans quelle mesure les services suivants vous intéresseraient-ils? Veuillez indiquer
votre degré d’intérêt pour chacun des services :

- (a) Ateliers sur les pratiques exemplaires en matière de gestion des données
- (b) Consultations personnalisées sur les pratiques de gestion des données à l’intention de groupes ou de projets de recherche précis;
- (c) Aide à la préparation de plans de gestion de données afin de satisfaire aux exigences des organismes subventionnaires;
- (d) Aide à la documentation et à la description de données (p. ex., création de métadonnées);
- (e) Aide concernant des enjeux éthiques ou juridiques (p. ex., confidentialité, protection de la vie privée, droits de propriété intellectuelle, anonymisation des données);
- (f) Offre d'un espace de stockage et de sauvegarde des données durant les projets de recherche en cours;
- (g) Conception d'un dépôt supporté par l'institution pour l’accès aux données de recherche et la conservation à long terme de celles-ci;
- (h) Aide au versement dans des dépôts de données appropriés;
- (i) Attribution d’identificateurs d’objets numériques (DOI) permanents pour les ensembles de données;
- (j) Aide au repérage de données de recherche ouvertes.

1. Très intéressé
2. Intéressé
3. Pas intéressé
4. Ne s’applique pas

Question 15
-----------

Veuillez indiquer votre statut.

1. Chercheur(e)
2. Enseignant(e) chercheur(e)
3. Professionnel(le) de recherche
4. Autre

Question 16
-----------

Veuillez indiqué votre instance de recherche:
1. CNIMI
2. CCEG
3. Cégep de Drummondville (autre que CCEG et CNIMI)
4. Autre

Question 17
-----------

Quelle(s) source(s) de financement avez-vous utilisée(s) au cours des trois
dernières années ou auxquelle(s) prévoyez-vous présenter une demande dans les
trois prochaines années? Choisissez toutes les réponses qui s’appliquent.

❓Est-ce qu'il y en a d'autres qui ne s'applique qu'au Collégial❓

1. Chaires de recherche du Canada
2. Conseil de recherches en sciences humaines (CRSH)
3. Conseil de recherches en sciences naturelles et en génie (CRSNG)
4. Fonds de recherche du Québec - Nature et technologies (FRQNT)
5. Fonds de recherche du Québec – Société et culture (FRQSC)
6. Fonds de recherche du Québec – Santé (FRQS)
7. Fonds de développement académique du réseau (FODAR)
8. Fonds institutionnels de la recherche (FIR)
9. Industrie et sociétés privées
10. Instituts de recherche en santé du Canada (IRSC)
11. Subvention dans le cadre du programme Savoir du CRSH
12. Subvention de développement de partenariats du CRSH
13. Autres agences et ministères fédéraux
14. Autres agences et ministères québécois
15. Fondations et organismes à but non lucratif (ex: MITACS)
16. Aucune
17. Autre

