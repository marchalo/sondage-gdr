Sondage Gestion des données de recherche
========================================

Ce dépôt contient les documents de travail à propos du sondage préliminaire aux
travaux du comité sur la gestion des données de recherche du Cégep de Drummondville.

Autors: Louis Marchand

Ce travail est sous license
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
