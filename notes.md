À noter
=======

À faire
-------
 - Créer deux documents Teams:
    * Sondage_UQTR 
    * Notre sondage
- Envoyer un message à Julie (CC Caroline)
    * pour avoir son opinion sur questions 5, 6, 7, 8, 9, 10
    * pour savoir si le sondage est à son goût et si elle veut ajouter/modifier/supprimer quelque chose

Autres notes
------------

- Il sera probablement nécessaire de créer un autre sondage pour les collaborateurs·trices techniques.
- La question 16 de l'QUTR devrait être mis dans un document (d'information ou de formation) pour les avantages du partage des données de recherche.

- Sujet de formation dans les questions de l'UQTR (question 19):
1. Je ne dirige pas d’étudiants gradués
2. Je n’aborde pas de sujets liés à la GDR
3. La sécurité des données
4. La confidentialité des données
5. Le contrôle des versions de données
6. La sauvegarde des données
7. L’éthique en matière de données
8. Le partage des données
9. La documentation des données
10. La conservation et l'archivage des données
